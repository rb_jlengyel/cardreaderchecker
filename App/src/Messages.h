#ifndef MESSAGES_H
#define MESSAGES_H

#define SEPARATOR "-------------------------------------------------------------------------------\n"
#define EOL "\n"

#define AUTHOR_MESSAGE "Esta aplicacio'n fue desarrollada por Juan Sebastian Lengyel de Analista I de Desarrollo de Recaudo Bogota' SAS (jlengyel@rbsas.co).\n"
#define APPLICATION_INSTRUCTIONS "El proposito de esta aplicacio'n es verificar la correcta comunicacio'n de la SAM tuLlave en el lector de referencia ACR1252U, y el funcionamiento del lector de tarjetas sin contacto.\nPara ejecutar la tarea deseada sencillamente digite el nu'mero de la tarea que desea realizar y a continucio'n oprima Enter\nLa aplicacio'n determinara' si la prueba fue exitosa o no, e indicará, en lo posible, pasos para corregir el error.\n"

#define MENU_HEADER "Escriba el nu'mero de la tarea que desea ejecutar, y a continuacio'n oprima Enter:\n"
#define MENU_CHECK_SAM_COMMUNICATION "%d. Verificar comunicacio'n con SAM\n"
#define MENU_CHECK_CARD_COMMUNICATION "%d. Verificar comunicacio'n con tarjeta\n"
#define MENU_EXIT_APPLICATION "%d. Salir\n"

#define CHECK_SAM_COMMUNICATION_INSTRUCTIONS "Asegurese que el lector se encuentra conectado al computador y que la SAM se encuentre en el slot del lector\n"
#define CHECK_SAM_COMMUNICATION_SUCCESFUL "Se ha detectado y comunicado correctamente con la SAM\n"
#define CHECK_SAM_COMMUNICATION_ERROR "Error al tratar de comunicarse con la SAM.\nEl co'digo de error es %X.\nSe recomienda:\n"

#define CHECK_CARD_COMMUNICATION_INSTRUCTIONS "Asegurese que el lector se encuentra conectado al computador y que haya una tarjeta sobre el lector\n"
#define CHECK_CARD_COMMUNICATION_SUCCESFUL "Se ha detectado y comunicado correctamente con la tarjeta\n"
#define CHECK_CARD_COMMUNICATION_ERROR "Error al tratar de comunicarse con la tarjeta.\nEl co'digo de error es %X.\nSe recomienda:\n"

#define SCARD_E_COMM_DATA_LOST_MESSAGE "Error de comunicación. Coloque o vuelva a colocar el elemento en el lugar apropiado.\n"
#define SCARD_E_DUPLICATE_READER_MESSAGE "Error de nombre de lector duplicado. Desconecte todos los lectores de la maquina y reconecte en el orden apropiado.\n"
#define SCARD_E_NO_READERS_AVAILABLE_MESSAGE "Error de deteccio'n del lector. No se detecta ningu'n lector conectado. Conecte el lector. Si reincide, instale o reinstale los controladores del lector.\n"
#define SCARD_E_NO_SERVICE_MESSAGE "El servicio de administración de recursos de lectores no está disponible. Solicite asistencia te'cnica.\n"
#define SCARD_E_NO_SMARTCARD_MESSAGE "No se ha encontrado una tarjeta o SAM. Coloquela o recoloquela en el lugar indicado.\n"
#define SCARD_E_READER_UNAVAILABLE_MESSAGE "El lector no se encuentra disponible. Cierre y termine otros procesos que puedan estar ocupando la comunicación con el lector.\n"
#define SCARD_E_SERVICE_STOPPED_MESSAGE "El servicio de administración de recursos de lectores se detuvo. Solicite asistencia te'cnica.\n"
#define SCARD_E_SHARING_VIOLATION_MESSAGE "El lector no se encuentra disponible. Cierre y termine otros procesos que puedan estar ocupando la comunicación con el lector.\n"
#define SCARD_E_TIMEOUT_MESSAGE "No se detecto' ninguna tarjeta. Coloque o recoloque el elemento en el sitio apropiado.\n"
#define SCARD_E_UNKNOWN_READER_MESSAGE "No se reconoce correctamente el lector. Desconectelo y vuelva a conectarlo.\n"
#define UNRECOGNIZED_ERROR_MESSAGE "No se reconoce el error. Anote el error y solicite asistencia te'cnica.\n"

#endif //MESSAGES_H