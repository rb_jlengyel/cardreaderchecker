#ifndef CARDCOMM_H
#define CARDCOMM_H

#define CL_READER_NAME "ACS ACR1252 1S CL Reader PICC 0"

#define CL_READER_MAX_WAIT_TIME 0

#define CARD_SW1_OK 0x90

#include <winscard.h>
#pragma comment(lib, "winscard.lib")

typedef struct CARD_RESPONSE
{
	BYTE data[300];					/*!< data of the APDU. */
	DWORD length;
} CARD_RESPONSE_t;

SCARDCONTEXT scc_CLReader;
DWORD m_dwActiveProtocolCARD;

int CARD_Command(SCARDHANDLE sch_CLCard, LPCBYTE pStrCommand, DWORD pIntLenCommand, CARD_RESPONSE_t* outCardResponse);
LONG ActivateComm_CLCard(SCARDHANDLE sch_CLCard);
void FinishComm_CLCard(SCARDHANDLE sch_CLCard);
int CheckMifareCLCard(SCARDHANDLE sch_CLCard);
void Remove_CLCard();

#endif