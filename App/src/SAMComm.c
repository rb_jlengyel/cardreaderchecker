#include "SAMComm.h"

LONG ActivateComm_SCCard(SCARDHANDLE sch_SCCard) {
	//-------------------------------------------------------------------------------
	// Declaration of local varibles
	//-------------------------------------------------------------------------------
	LONG rc;							//Winscard response code
	SCARD_READERSTATE sReaderState;		//Reader state handler
	//-------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------
	// Establish context with card reader
	//-------------------------------------------------------------------------------
	rc = SCardEstablishContext(
		SCARD_SCOPE_SYSTEM,				//In DWORD Scope of resource manager context
		NULL,							//RFU
		NULL,							//RFU
		&scc_SCReader);					//Smartcard context

	if (rc != SCARD_S_SUCCESS)
	{
		//printf("CARD_ActivateCommCL fails at SCardEstablishContext with error code: %Xh\n", rc);
		return rc;
	}
	//-------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------
	// Initialize sReaderState
	//-------------------------------------------------------------------------------
	sReaderState.szReader = SC_READER_NAME;
	sReaderState.dwCurrentState = SCARD_STATE_UNAWARE;
	sReaderState.dwEventState = SCARD_STATE_UNAWARE;
	//-------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------
	// Get smartcard status and attempt to connect
	//-------------------------------------------------------------------------------
	//Update card reader status
	rc = SCardGetStatusChange(
		scc_SCReader,						//Smartcard context
		0,								//Maximum amount of time (in milliseconds) to wait for an action
		&sReaderState,					//Reader state
		1);								//Number of readers

	sReaderState.dwCurrentState = sReaderState.dwEventState;
	//-------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------
	// The SCardGetStatusChange function blocks execution until the current 
	// availability of the cards in a specific set of readers changes
	//-------------------------------------------------------------------------------
	if ((sReaderState.dwEventState & SCARD_STATE_PRESENT) != SCARD_STATE_PRESENT)
	{
		rc = SCardGetStatusChange(
			scc_SCReader,						//Resource manager handle
			SC_READER_MAX_WAIT_TIME,			//Maximum amount of time (in milliseconds) to wait for an action
			&sReaderState,					//Reader state
			1);								//Number of readers

		sReaderState.dwCurrentState = sReaderState.dwEventState;
	}
	//-------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------
	// Connect with the card if it is present
	//-------------------------------------------------------------------------------
	if ((sReaderState.dwEventState & SCARD_STATE_PRESENT) == SCARD_STATE_PRESENT)
	{
		//Establishes a connection to a smart card contained by a specific reader
		rc = SCardConnect(
			scc_SCReader,					//Resource manager handle
			SC_READER_NAME,				//Reader name
			SCARD_SHARE_EXCLUSIVE,		//Share Mode
			SCARD_PROTOCOL_Tx,			//Preferred protocols (T = 0 or T = 1)
			&sch_SCCard,				//Returns the card handle
			&m_dwActiveProtocolCARD);	//Active protocol

		if (rc != SCARD_S_SUCCESS)
		{
			//printf("CARD_ActivateCommCL fails at SCardConnect with error code: %Xh\n", rc);
			SCardDisconnect(scc_SCReader, SCARD_LEAVE_CARD);
			SCardReleaseContext(scc_SCReader);
			return rc;
		}
		return rc;
	}

	return rc;
	//-------------------------------------------------------------------------------
}

void FinishComm_SCCard(SCARDHANDLE sch_SCCard) {
	//-------------------------------------------------------------------------------
	// Declaration of local varibles
	//-------------------------------------------------------------------------------
	LONG rc;		//Winscard response code
	//-------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------
	// Disconnect and release reader context
	//-------------------------------------------------------------------------------
	rc = SCardDisconnect(sch_SCCard, SCARD_LEAVE_CARD);
	if (rc != SCARD_S_SUCCESS)
	{
		//printf("CARD_FinishComm fails at SCardDisconnect with error code: %Xh\n", rc);
	}

	rc = SCardReleaseContext(scc_SCReader);
	if (rc != SCARD_S_SUCCESS)
	{
		//printf("CARD_FinishComm fails at SCardReleaseContext with error code: %Xh\n", rc);
	}
	//-------------------------------------------------------------------------------
}

int SAM_Command(SCARDHANDLE sch_SCCard, LPCBYTE pStrCommand, DWORD pIntLenCommand, SAM_RESPONSE_t* outSAMResponse) {
	LPCSCARD_IO_REQUEST ioRequest; // Pointer to the send protocol header.
	LONG lRetValue; //------------------------------

	BYTE baResponseApdu[300]; //Response
	BYTE GetResponse[5]; // Get Response Buffer
	DWORD Responselen = 300; //ResponseLen

	BYTE baResponseApdu1[300]; //Response
	DWORD Responselen1 = 300; //ResponseLen

	ioRequest = SCARD_PCI_T0;
	lRetValue = SCardBeginTransaction(sch_SCCard);
	//------------------------------------------------------------------------
	lRetValue = SCardTransmit(sch_SCCard,		// Card handle.
		ioRequest,		// Pointer to the send protocol header.
		pStrCommand,	// Send buffer.
		pIntLenCommand,	// Send buffer length.
		NULL,			// Pointer to the rec. protocol header.
		baResponseApdu,	// Receive buffer.
		&Responselen);	// Receive buffer length.
	//-------------------------------------------------------------------------

	if (SCARD_S_SUCCESS != lRetValue)
	{
		return 0;
	}

	if (Responselen == 2)
	{
		if (baResponseApdu[0] == SAM_SW1_OK && baResponseApdu[1] == 0x00)
			return 1;

		if (baResponseApdu[0] != SAM_SW1_GET_RESPONSE)
		{
			return 0;
		}
		else
		{
			memcpy(GetResponse, SAM_GET_RESPONSE, SAM_GET_RESPONSE_LEN);
			GetResponse[4] = baResponseApdu[1];
			//------------------------------------------------------------------------
			lRetValue = SCardTransmit(sch_SCCard,		// Card handle.
				ioRequest,		// Pointer to the send protocol header.
				GetResponse,	// Send buffer.
				SAM_GET_RESPONSE_LEN + 1,	// Send buffer length.
				NULL,			// Pointer to the rec. protocol header.
				baResponseApdu1,	// Receive buffer.
				&Responselen1);	// Receive buffer length.
			//-------------------------------------------------------------------------
			if (SCARD_S_SUCCESS != lRetValue)
			{
				return 0;
			}

			if (Responselen1 == 2)
			{
				if (baResponseApdu1[0] != SAM_SW1_GET_RESPONSE)
				{
					return 0;
				}
			}
			if (baResponseApdu1[Responselen1 - 2] == SAM_SW1_OK)
			{
				lRetValue = SCardEndTransaction(sch_SCCard, SCARD_LEAVE_CARD);
				memcpy(outSAMResponse->data, baResponseApdu1, Responselen1 - 2);
				outSAMResponse->length = Responselen1 - 2;
				return 1;
			}
			else
				return 0;
		}
	}

	//--------------------------------------------------------------------------
	//Return Response
	//---------------------------------------------------------------------------
	lRetValue = SCardEndTransaction(sch_SCCard, SCARD_LEAVE_CARD);
	memcpy(outSAMResponse->data, baResponseApdu, Responselen);
	outSAMResponse->length = Responselen;
	return 1;
}

void Remove_SCCard() {
	//-------------------------------------------------------------------------------
	// Declaration of local varibles
	//-------------------------------------------------------------------------------
	LONG rc;							//Winscard response code
	SCARD_READERSTATE sReaderState;		//Reader state handler
	//-------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------
	// Initialize sReaderState
	//-------------------------------------------------------------------------------
	sReaderState.szReader = SC_READER_NAME;
	sReaderState.dwCurrentState = SCARD_STATE_UNAWARE;
	sReaderState.dwEventState = SCARD_STATE_UNAWARE;
	//-------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------
	// Get smartcard status and attempt to connect
	//-------------------------------------------------------------------------------
	//Update card reader status
	rc = SCardGetStatusChange(
		scc_SCReader,						//Smartcard context
		0,								//Maximum amount of time (in milliseconds) to wait for an action
		&sReaderState,					//Reader state
		1);								//Number of readers

	sReaderState.dwCurrentState = sReaderState.dwEventState;
	//-------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------
	// The SCardGetStatusChange function blocks execution until the current 
	// availability of the cards in a specific set of readers changes
	//-------------------------------------------------------------------------------
	if ((sReaderState.dwEventState & SCARD_STATE_PRESENT) == SCARD_STATE_PRESENT)
	{
		rc = SCardGetStatusChange(
			scc_SCReader,						//Resource manager handle
			INFINITE,			//Maximum amount of time (in milliseconds) to wait for an action
			&sReaderState,					//Reader state
			1);								//Number of readers

		sReaderState.dwCurrentState = sReaderState.dwEventState;
	}
	//-------------------------------------------------------------------------------
}