#include <stdio.h>
#include <stdlib.h>
#include "CardReaderChecker.h"
#include "Messages.h"

void main()
{
	int input;
	char in[sizeof(int)];

	SCARDHANDLE card_Handler = NULL;
	LONG result;

	printf(SEPARATOR);
	printf(AUTHOR_MESSAGE);
	printf(SEPARATOR);

	printf(APPLICATION_INSTRUCTIONS);

	for (;;)
	{
		printf(SEPARATOR);
		printf(MENU_HEADER);
		printf(MENU_CHECK_SAM_COMMUNICATION, CHECK_SAM_COMMUNICATION_OPTION);
		printf(MENU_CHECK_CARD_COMMUNICATION, CHECK_CARD_COMMUNICATION_OPTION);
		printf(MENU_EXIT_APPLICATION, EXIT_APPLICATION_OPTION);
		printf(SEPARATOR);

		fgets(in, sizeof(int), stdin);
		sscanf(in, "%d", &input);
		fgets(NULL, NULL, stdin);

		switch (input)
		{
		case CHECK_SAM_COMMUNICATION_OPTION:
			printf(CHECK_SAM_COMMUNICATION_INSTRUCTIONS);
			system("pause");

			result = ActivateComm_SCCard(card_Handler);

			if (result != SCARD_S_SUCCESS)
			{
				printf(CHECK_SAM_COMMUNICATION_ERROR, result);
				showErrorMessage(result);
			}
			else
			{
				printf(CHECK_SAM_COMMUNICATION_SUCCESFUL);
				FinishComm_SCCard(card_Handler);
			}
			system("pause");
			break;

		case CHECK_CARD_COMMUNICATION_OPTION:
			printf(CHECK_CARD_COMMUNICATION_INSTRUCTIONS);
			system("pause");

			result = ActivateComm_CLCard(card_Handler);

			if (result != SCARD_S_SUCCESS)
			{
				printf(CHECK_CARD_COMMUNICATION_ERROR, result);
				showErrorMessage(result);
			}
			else
			{
				printf(CHECK_CARD_COMMUNICATION_SUCCESFUL);
				FinishComm_CLCard(card_Handler);
			}
			system("pause");
			break;

		case EXIT_APPLICATION_OPTION:
			return;
		}
	}
}

void showErrorMessage(LONG result)
{
	switch (result)
	{
	case SCARD_E_COMM_DATA_LOST:
		printf(SCARD_E_COMM_DATA_LOST_MESSAGE);
		break;
	case SCARD_E_DUPLICATE_READER:
		printf(SCARD_E_DUPLICATE_READER_MESSAGE);
		break;
	case SCARD_E_NO_READERS_AVAILABLE:
		printf(SCARD_E_NO_READERS_AVAILABLE_MESSAGE);
		break;
	case SCARD_E_NO_SERVICE:
		printf(SCARD_E_NO_SERVICE_MESSAGE);
		break;
	case SCARD_E_NO_SMARTCARD:
		printf(SCARD_E_NO_SMARTCARD_MESSAGE);
		break;
	case SCARD_E_READER_UNAVAILABLE:
		printf(SCARD_E_READER_UNAVAILABLE_MESSAGE);
		break;
	case SCARD_E_SERVICE_STOPPED:
		printf(SCARD_E_SERVICE_STOPPED_MESSAGE);
		break;
	case SCARD_E_SHARING_VIOLATION:
		printf(SCARD_E_SHARING_VIOLATION_MESSAGE);
		break;
	case SCARD_E_TIMEOUT:
		printf(SCARD_E_TIMEOUT_MESSAGE);
		break;
	case SCARD_E_UNKNOWN_READER:
		printf(SCARD_E_UNKNOWN_READER_MESSAGE);
		break;
	default:
		printf(UNRECOGNIZED_ERROR_MESSAGE);
		break;
	}
}