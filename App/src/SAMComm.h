#ifndef SAMCOMM_H
#define SAMCOMM_H

#define SC_READER_NAME "ACS ACR1252 1S CL Reader SAM 0"

#define SC_READER_MAX_WAIT_TIME 0

#define SAM_SW1_OK 0x90
#define SAM_SW1_GET_RESPONSE 0x61

#define SAM_GET_RESPONSE						"\x00\xC0\x00\x00"
#define SAM_GET_RESPONSE_LEN					4

#include <winscard.h>
#pragma comment(lib, "winscard.lib")

typedef struct SAM_RESPONSE
{
	BYTE data[300];					/*!< data of the APDU. */
	DWORD length;
} SAM_RESPONSE_t;

SCARDCONTEXT scc_SCReader;
DWORD m_dwActiveProtocolCARD;

int SAM_Command(SCARDHANDLE sch_SCCard, LPCBYTE pStrCommand, DWORD pIntLenCommand, SAM_RESPONSE_t* outSAMResponse);
LONG ActivateComm_SCCard(SCARDHANDLE sch_SCCard);
void FinishComm_SCCard(SCARDHANDLE sch_SCCard);
void Remove_SCCard();

#endif