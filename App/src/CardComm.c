#include <stdio.h>
#include "CardComm.h"

LONG ActivateComm_CLCard(SCARDHANDLE sch_CLCard) {
	//-------------------------------------------------------------------------------
	// Declaration of local varibles
	//-------------------------------------------------------------------------------
	LONG rc;							//Winscard response code
	SCARD_READERSTATE sReaderState;		//Reader state handler
	//-------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------
	// Establish context with card reader
	//-------------------------------------------------------------------------------
	rc = SCardEstablishContext(
		SCARD_SCOPE_SYSTEM,				//In DWORD Scope of resource manager context
		NULL,							//RFU
		NULL,							//RFU
		&scc_CLReader);					//Smartcard context

	if (rc != SCARD_S_SUCCESS)
	{
		//printf("CARD_ActivateCommCL fails at SCardEstablishContext with error code: %Xh\n", rc);
		return rc;
	}
	//-------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------
	// Initialize sReaderState
	//-------------------------------------------------------------------------------
	sReaderState.szReader = CL_READER_NAME;
	sReaderState.dwCurrentState = SCARD_STATE_UNAWARE;
	sReaderState.dwEventState = SCARD_STATE_UNAWARE;
	//-------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------
	// Get smartcard status and attempt to connect
	//-------------------------------------------------------------------------------
	//Update card reader status
	rc = SCardGetStatusChange(
		scc_CLReader,						//Smartcard context
		0,								//Maximum amount of time (in milliseconds) to wait for an action
		&sReaderState,					//Reader state
		1);								//Number of readers

	sReaderState.dwCurrentState = sReaderState.dwEventState;
	//-------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------
	// The SCardGetStatusChange function blocks execution until the current 
	// availability of the cards in a specific set of readers changes
	//-------------------------------------------------------------------------------
	if ((sReaderState.dwEventState & SCARD_STATE_PRESENT) != SCARD_STATE_PRESENT)
	{
		rc = SCardGetStatusChange(
			scc_CLReader,						//Resource manager handle
			CL_READER_MAX_WAIT_TIME,			//Maximum amount of time (in milliseconds) to wait for an action
			&sReaderState,					//Reader state
			1);								//Number of readers

		sReaderState.dwCurrentState = sReaderState.dwEventState;
	}
	//-------------------------------------------------------------------------------
	
	//-------------------------------------------------------------------------------
	// Connect with the card if it is present
	//-------------------------------------------------------------------------------
	if ((sReaderState.dwEventState & SCARD_STATE_PRESENT) == SCARD_STATE_PRESENT)
	{
		//Establishes a connection to a smart card contained by a specific reader
		rc = SCardConnect(
			scc_CLReader,					//Resource manager handle
			CL_READER_NAME,				//Reader name
			SCARD_SHARE_EXCLUSIVE,		//Share Mode
			SCARD_PROTOCOL_Tx,			//Preferred protocols (T = 0 or T = 1)
			&sch_CLCard,				//Returns the card handle
			&m_dwActiveProtocolCARD);	//Active protocol

		if (rc != SCARD_S_SUCCESS)
		{
			SCardDisconnect(scc_CLReader, SCARD_LEAVE_CARD);
			SCardReleaseContext(scc_CLReader);
			return rc;
		}
		return rc;
	}

	return rc;
	//-------------------------------------------------------------------------------
}

void FinishComm_CLCard(SCARDHANDLE sch_CLCard) {
	//-------------------------------------------------------------------------------
	// Declaration of local varibles
	//-------------------------------------------------------------------------------
	LONG rc;		//Winscard response code
	//-------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------
	// Disconnect and release reader context
	//-------------------------------------------------------------------------------
	rc = SCardDisconnect(sch_CLCard, SCARD_LEAVE_CARD);
	if (rc != SCARD_S_SUCCESS)
	{
		//printf("CARD_FinishComm fails at SCardDisconnect with error code: %Xh\n", rc);
	}

	rc = SCardReleaseContext(scc_CLReader);
	if (rc != SCARD_S_SUCCESS)
	{
		//printf("CARD_FinishComm fails at SCardReleaseContext with error code: %Xh\n", rc);
	}
	//-------------------------------------------------------------------------------
}

int CARD_Command(SCARDHANDLE sch_CLCard, LPCBYTE pStrCommand, DWORD pIntLenCommand, CARD_RESPONSE_t* outCardResponse) {
	LPCSCARD_IO_REQUEST  ioRequest; // Pointer to the send protocol header.
	LONG lRetValue; //------------------------------

	BYTE baResponseApdu[300]; //Response
	DWORD Responselen = 300; //ResponseLen

	ioRequest = SCARD_PCI_T1;
	lRetValue = SCardBeginTransaction(sch_CLCard);
	//------------------------------------------------------------------------
	lRetValue = SCardTransmit(sch_CLCard,		// Card handle.
		ioRequest,		// Pointer to the send protocol header.
		pStrCommand,	// Send buffer.
		pIntLenCommand,	// Send buffer length.
		NULL,			// Pointer to the rec. protocol header.
		baResponseApdu,	// Receive buffer.
		&Responselen);	// Receive buffer length.
	//-------------------------------------------------------------------------
	if (SCARD_S_SUCCESS != lRetValue)
	{
		return 0;
	}
	//--------------------------------------------------------------------------
	//Return Response
	//---------------------------------------------------------------------------
	if (baResponseApdu[Responselen - 2] == CARD_SW1_OK)
	{
		lRetValue = SCardEndTransaction(sch_CLCard, SCARD_LEAVE_CARD);
		memcpy(outCardResponse->data, baResponseApdu, Responselen - 2);
		outCardResponse->length = Responselen - 2;
		return 1;
	}

	return 0;
}

int CheckMifareCLCard(SCARDHANDLE sch_CLCard) {
	//-------------------------------------------------------------------------------
	// Declaration of local variables
	//-------------------------------------------------------------------------------
	LPBYTE pbAttr = NULL;
	DWORD cByte = SCARD_AUTOALLOCATE;
	LONG rc;
	//-------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------
	// Declaration of constants to be used in the method
	//-------------------------------------------------------------------------------
	const BYTE baAtrMifare1K[] = {
		0x3B,							// Initial
		0x8F,							// T0
		0x80,							// TD1
		0x01,							// TD2
		0x80,							// Category Indicator
		0x4F,							// Appl. Id. Precence Indicator
		0x0C,							// Tag Length
		0xA0, 0x00, 0x00, 0x03, 0x06,	// AID
		0x03,							// SS
		0x00, 0x01,						// NN - Mifare 1k
		0x00, 0x00, 0x00, 0x00,			// r.f.u
		0x6A };							// TCK

	const BYTE baAtrMifare4K[] = {
		0x3B,							// Initial
		0x8F,							// T0
		0x80,							// TD1
		0x01,							// TD2
		0x80,							// Category Indicator
		0x4F,							// Appl. Id. Precence Indicator
		0x0C,							// Tag Length
		0xA0, 0x00, 0x00, 0x03, 0x06,	// AID
		0x03,							// SS
		0x00, 0x02,						// NN - Mifare 4k
		0x00, 0x00, 0x00, 0x00,			// r.f.u
		0x69 };							// TCK
	//-------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------
	// Gets the current reader attributes for the given handle
	//-------------------------------------------------------------------------------
	rc = SCardGetAttrib(
		sch_CLCard,				// Card handle.
		SCARD_ATTR_ATR_STRING,	// Attribute identifier.
		&pbAttr,					// Attribute buffer.
		&cByte);				// Returned attribute length.
	//-------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------
	// Check if card is 1K MIFARE or 4K MIFARE
	//-------------------------------------------------------------------------------
	if (cByte != 20)
	{
		return 0;
	}

	if (memcmp(baAtrMifare1K, pbAttr, sizeof(baAtrMifare1K)) != 0 && memcmp(baAtrMifare4K, pbAttr, sizeof(baAtrMifare4K)) != 0)
	{
		return 0;
	}
	return 1;
	//-------------------------------------------------------------------------------
}

void Remove_CLCard() {
	//-------------------------------------------------------------------------------
	// Declaration of local varibles
	//-------------------------------------------------------------------------------
	LONG rc;							//Winscard response code
	SCARD_READERSTATE sReaderState;		//Reader state handler
	//-------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------
	// Initialize sReaderState
	//-------------------------------------------------------------------------------
	sReaderState.szReader = CL_READER_NAME;
	sReaderState.dwCurrentState = SCARD_STATE_UNAWARE;
	sReaderState.dwEventState = SCARD_STATE_UNAWARE;
	//-------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------
	// Get smartcard status and attempt to connect
	//-------------------------------------------------------------------------------
	//Update card reader status
	rc = SCardGetStatusChange(
		scc_CLReader,						//Smartcard context
		0,								//Maximum amount of time (in milliseconds) to wait for an action
		&sReaderState,					//Reader state
		1);								//Number of readers

	sReaderState.dwCurrentState = sReaderState.dwEventState;
	//-------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------
	// The SCardGetStatusChange function blocks execution until the current 
	// availability of the cards in a specific set of readers changes
	//-------------------------------------------------------------------------------
	if ((sReaderState.dwEventState & SCARD_STATE_PRESENT) == SCARD_STATE_PRESENT)
	{
		rc = SCardGetStatusChange(
			scc_CLReader,						//Resource manager handle
			INFINITE,			//Maximum amount of time (in milliseconds) to wait for an action
			&sReaderState,					//Reader state
			1);								//Number of readers

		sReaderState.dwCurrentState = sReaderState.dwEventState;
	}
	//-------------------------------------------------------------------------------
}